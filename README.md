# adb pull junks shell script

adb_pull_junks.sh is a emergency shell script to get files from bootlooping Android device using the ADB (Android Debug Bridge).
files are splitted. only parts are pulled. if usb connection is interrupted, the script continues with current part.

usage:
open the file with Editor and set the directory to pull
Note: Remove leading `/' from member names
dir=sdcard/TWRP

run the script (GNU linux only)
./adb_pull_junks.sh

wait for background tasks
