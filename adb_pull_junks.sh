#!/bin/sh

# adb_pull_junks.sh emergency shell script
# Copyright (C) 2023  alecxs @ XDA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Note: Remove leading `/' from member names
dir=sdcard/TWRP

junksize=52428800
blocksz=1048576

# Begin of declaration

########################################################

# getsize() function - print the file size of remote file
# usage: getsize <FILE>
getsize () {
  adb shell -n "stat -c%s \"$1\""
}

# getjunk() function - adb pull junk number of split file
# usage: getjunk <FILE> <NUMBER> <PARTS> <LAST_JUNK_SIZE>
getjunk () {
  sectors=$(expr ${junksize:-512} / ${blocksz:-512})
  mkdir -p "${1%/*}" || return 1;
  if [ $((${2:-0}+1)) -eq ${3:-0} ]; then
    [ ${4:-0} -eq 0 ] && filesize=${junksize:-0} || filesize=${4:-0}
  else
    filesize=${junksize:-0}
  fi
  [ -f "${1}-junk~${2:-0}" ] && [ $(stat -c%s "${1}-junk~${2:-0}") -eq ${filesize:-0} ] && return 0
  adb shell -n "dd bs=${blocksz:-512} skip=$(expr ${2:-0} \* ${sectors:-1}) count=${sectors:-1} if=\"$1\" 2> /dev/null" > "${1}-junk~${2:-0}" || return 1
  return 0
}

# splitt() function - adb pull split file
# usage: splitt <FILE> <SIZE>
splitt () {
  parts=$(expr $2 / ${junksize:-1})
  rest=$(expr $2 % ${junksize:-1})
  [ ${rest:-0} -gt 0 ] && parts=$((parts+1))
  echo "$1"
  i=0
  while [ ${i:-0} -lt $parts ]; do
    echo -n "\rpart $((i+1))/$parts "
    getjunk "$1" ${i:-0} $parts ${rest:-0}
    if [ $? != 0 ]; then
      echo "retrying..."
      echo "- waiting for device -"
      adb wait-for-any
      i=$((i-1))
    else
      i=$((i+1))
    fi
  done
  return 0;
}

# digest() function - checking digest for local file
# usage: digest <FILE>
digest () {
  if [ -f "$1" ] && [ -f "$1.sha2" ]; then
    cd "${1%/*}"
    sha256sum -c "${1##*/}.sha2"
    if [ $? != 0 ]; then
      cd -
      return 1
    else
      cd -
      return 0
    fi
  fi
  return 1
}

# merge() function - concatenate junks of split file (background task)
# usage: merge <FILE> &
merge () {
  find "${1%/*}" -maxdepth 1 -type f -name "${1##*/}-junk*" -print0 | sort -z -t '~' -k2 -n | xargs -0 cat > "$1"
}

# End of declaration

########################################################

# Begin of shell script

[ $(expr ${junksize:-1} % ${blocksz:-2}) -gt 0 ] && echo "Error: junksize=${junksize:-0} not divisible by blocksz=${blocksz:-0}" && exit 1;
echo "- waiting for device -"
adb wait-for-any
adb shell -n "find \"$dir\" -type f | sort" > index.txt

while read file; do
  size=$(getsize "$file")
  if [ -f "$file" ] && [ -f "$file.sha2" ]; then
    echo "${file##*/}: digest found. checking"
    digest "$file" && continue
  fi
  if [ ${size:-0} -gt $(expr ${junksize:-0} \* 12 / 10) ]; then
    echo "${file##*/}: is large, splitting"
    splitt "$file" ${size:-0} || exit 1;
    merge "$file" &
  else
    mkdir -p "${file%/*}" || return 1;
    adb pull -a "$file" "$file"
  fi
done < index.txt
echo -e "\n\nNote: some background tasks still running, don't kill that shell...\n\n"
